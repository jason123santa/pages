# nick = jason
# url  = https://jasonsanta.xyz/twtxt.txt
# avatar = https://jasonsanta.xyz/images/avatar.png
# description = I am jasonsanta or jason123santa or jason123 and I have a website over at https://jasonsanta.xyz
# link = Website https://jasonsanta.xyz
2022-08-19T00:36:20Z	testing twtxt using jenny
2022-08-29T17:11:25Z	Right now I have to setup jenny for my timeline. Just added myself to the Registry so that part is done.
2022-08-30T18:18:26Z	(#xqwi3za) @<akoizumi https://social.kyoko-project.wer.ee/user/akoizumi/twtxt.txt> yo does this work? 
2022-08-30T18:35:54Z	(#xqwi3za) @<xuu https://txt.sour.is/user/xuu/twtxt.txt> hey
2022-08-30T23:20:28Z	(#xqwi3za) @<akoizumi https://social.kyoko-project.wer.ee/user/akoizumi/twtxt.txt> Cool if I end up just running my own yarn pod but for now twtxt on my site hosted by codeberg works fine
2022-08-30T23:33:53Z	(#5dyovjq) @<akoizumi https://social.kyoko-project.wer.ee/user/akoizumi/twtxt.txt> cool to see that you fixed the css
2022-08-30T23:36:15Z	got jenny setup and threads works completly fine but now I want to figure out how to get auto publishing working
2022-08-30T23:37:28Z	(#7q5b5na) test again to see if it works
2022-08-30T23:38:20Z	(#7q5b5na) last test and if this does not work then I just have to run the git commands myself then
2022-08-30T23:45:46Z	(#7q5b5na) last test i am done testing now
2022-08-30T23:49:31Z	autopublishing works with jenny + mutt I think i am done configuring mostly everything now
2022-08-31T04:45:52Z	(#xqwi3za) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Thanks
2022-08-31T04:47:22Z	(#rle7lwq) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Yeah I can try and do that for yarn users.
2022-08-31T04:57:54Z	(#7q5b5na) @<prologic https://twtxt.net/user/prologic/twtxt.txt> I do think the post about how to setup jenny + mutt over on the uninformativ.de blog is still a great post. I used that post to see the steps to set it up and it works fine. Though I can write some blog post with some more documentation for things like auto publishing. The big issue with plain twtxt is that I would have not seen your post unless I looked on twtxt.net when I was looking at yarn a little bit more. Twtxt does overcome the issue by introducing the registry but I can't figure out any way to use them for Jenny and almost no one uses them in the first place. So I can't see anyones replies or mentions unless I am following them. Yarn does overcome the issue by friends of friends as you would know as the creator of yarn.
2022-08-31T13:17:54Z	(#yjdijwa) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Yeah I don't even know how to use them once I added myself to the registries. The jarn search engine is similar to the registries thing but its easier to search and find things from. Also I assume its easier to use it in the yarn pods and whatever elese to get new posts. I would always like to see yarn work with regular twtxt because there is advantges to plain twtxt.
2022-08-31T13:21:46Z	(#rle7lwq) @<prologic https://twtxt.net/user/prologic/twtxt.txt> I don't think Jenny does much of anything with the avatar and description but I do know yarn does and its not a bad thing to include the metadata for those users.
2022-08-31T13:27:30Z	(#7q5b5na) @<movq https://uninformativ.de/twtxt.txt> There might not even be any benefit to moving that to yarn.social. Besides the yarn site should promote using yarn not twtxt even though there is a little bit of information on the site.
2022-08-31T13:50:06Z	I have set an avatar and a description so there is that. Yarn users should benefit from the change
2022-08-31T15:45:39Z	(#rle7lwq) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Yeah I saw my profile picture on the yarn pod. Its nice that there is also mobile apps for yarn.
2022-08-31T16:06:21Z	(#7q5b5na) @<prologic https://twtxt.net/user/prologic/twtxt.txt> I never tried out any of the other clients except jenny with mutt. The best thing about yarn vs something like Mastodon is that its more promoted of the specification of twtxt files instead of server part. Twtxt can be hosted on some free static site host or some git server even so its really low resouces. Just a basic text file. As far as I know yarn is mostly just a web ui around twtxt and an extnetion to the specification to add some more usability and modern things. Anyone can join decentrilized network by having a twtxt file somewhere. If you want to support the specification of twtxt then that is really something most projects don't do and they promote the server software mostly.
2022-08-31T16:39:36Z	(#3v4bf4a) @<prologic https://twtxt.net/user/prologic/twtxt.txt> I have added that to my twtxt file. Even if I can't see the metadata the people from yarn can. Not a big deal to add the metadata and it helps yarn users
2022-08-31T16:43:01Z	(#7q5b5na) @<prologic https://twtxt.net/user/prologic/twtxt.txt> There is also a link to the blog post on the yarn.social site about how to setup jenny with mutt.
2022-08-31T16:53:19Z	(#yjdijwa) @<prologic https://twtxt.net/user/prologic/twtxt.txt> That is why yarn is better then something like activity pub. Everything over on activity pub tries to work with Mastodon not because its better but because its the most popular. Twtxt clients on the other hand tries to work with the yarn additions because most of the additions improve things even for twtxt users.
2022-08-31T17:06:47Z	@<movq https://uninformativ.de/twtxt.txt> Do you know how I would find people that reply to my posts or replies or even mention my users? Prologic tried to contact me and unless I found him on the yarn pod then I would not know he exists and wants to talk to me. The user agents would work but I don't know if I can view my web server logs from codeberg pages and I don't know how to monitor my logs for mentions. What about the way yarn does it by added people you follow to your twtxt file and having friends of friends like yarn does it be a thing for jenny. Just an idea
2022-08-31T18:32:13Z	The thing is I don't know how to search the web logs on Codeberg or even if they are public. That is the issue with just regular text files. The thing with having the follower list in the twtxt file is that then it knows to track friends of friends like with yarn. 
If not having www is an issue when I will add it in. Good to know its something I have to change
2022-08-31T18:41:06Z	(#ssfxx4q) @<movq https://uninformativ.de/twtxt.txt> I did not mention you and instead of replying I made a new post oops.
2022-08-31T22:42:06Z	(#3pwbaza) @<akoizumi https://social.kyoko-project.wer.ee/user/akoizumi/twtxt.txt> We were talking about having links on the yarn.social page for some guide on how to setup jenny + mutt
2022-09-01T01:40:16Z	(#7g3ezoa) @<prologic https://twtxt.net/user/prologic/twtxt.txt> First I would have to be able to get to my logs and I don't think i can
2022-09-01T01:43:36Z	(#3v4bf4a) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Yeah I do think Jenny adds most of the extensions.
2022-09-01T01:45:24Z	(#7q5b5na) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Your the one who wrote the page and did not even realise it was there. lol how nice (not a bad thing just funny)
2022-09-01T02:00:55Z	(#7g3ezoa) @<prologic https://twtxt.net/user/prologic/twtxt.txt> I don't know any other way to host my file at my domain unless I make a sub domain. I am going to ask codeberg if they offer access of logs.
2022-09-01T03:16:34Z	(#7g3ezoa) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Yeah this is hosted at codeberg pages. Yeah the same issue would happen with github pages.
2022-09-01T03:45:21Z	(#u4cn57a) @<prologic https://twtxt.net/user/prologic/twtxt.txt> I don't know how to code in go or anything really. Not even really know how to do html and css only basic things.
2022-09-01T03:45:59Z	(#dtqwa2a) @<prologic https://twtxt.net/user/prologic/twtxt.txt> I joined so its better with me lol
2022-09-01T13:10:15Z	(#dtqwa2a) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Yeah I am not one of these people who just have a twtxt file and end up posting a few things but not interacting with anyone. I do want to interact with the people of twtxt and yarn users. And not just twtxt users but I do care somewhat about the yarn users because really yarn is twtxt but with additions to make the experience better and a webui and the such like multi users. On top of that yarn and the twtxt clients add things like threads that are even helpful for twtxt users.
2022-09-01T13:11:15Z	(#u4cn57a) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Yeah I don't know how I am going to know if someone wants to talk with me but I guess for now twtxt works.
2022-09-01T18:18:22Z	(#u4cn57a) @<lyse https://lyse.isobeef.org/twtxt.txt> I never seen anyone mention wrong
2022-09-02T01:53:23Z	(#dtqwa2a) @<prologic https://twtxt.net/user/prologic/twtxt.txt> nope I don't know why its yarn
2022-09-02T02:57:09Z	(#gpemesq) @<prologic https://twtxt.net/user/prologic/twtxt.txt> I guess anything that is open source and is not resctricted can be forked. I would assume jenny is the same but I can wait will movq replies.
2022-09-02T03:29:26Z	(#wu3gjsa) @<prologic https://twtxt.net/user/prologic/twtxt.txt> That is cool. The name is cool. Like saying "yarn ballers" but that sounds weird and cool at the same time
2022-09-02T03:30:35Z	(#73yy6na) @<prologic https://twtxt.net/user/prologic/twtxt.txt> In jenny there is the option to reply to forked threads
2022-09-02T15:12:32Z	(#orerkha) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Yeah I never used it before
2022-09-02T15:15:03Z	(#mykjx3a) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Those sounds really cool. 
Is something like "reyarn" a thing or not a thing?
2022-09-02T16:19:11Z	(#73yy6na) @<prologic https://twtxt.net/user/prologic/twtxt.txt> I don't think I have used forked threads before
2022-09-02T16:21:09Z	(#orerkha) @<prologic https://twtxt.net/user/prologic/twtxt.txt> I think now I know what forked threads are and figured out how to use them but i don't really know
2022-09-02T23:09:08Z	(#4ugowcq) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Its a way to make another conversation in a thread of posts?
2022-09-03T00:13:54Z	(#4ugowcq) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Nice. Now I know about forking replies.
2022-09-03T00:16:43Z	(#iayikua) @<prologic https://twtxt.net/user/prologic/twtxt.txt> I think those are fine because its just sharing someone elses post to people who follow you. Those people who follow you might not follow the orginal person and in return might never see that post unless its retoos/retweets. The thing that is harmful is likes.
2022-09-03T01:51:26Z	(#iayikua) @<prologic https://twtxt.net/user/prologic/twtxt.txt> I guess as long as jenny just links to the post.
2022-09-03T01:56:20Z	(#iayikua) @<prologic https://twtxt.net/user/prologic/twtxt.txt> That looks nice. So that if you link the post in another app it will show the message
2022-09-03T01:57:59Z	(#iayikua) @<lyse https://lyse.isobeef.org/twtxt.txt> Responding could be harmful
2022-09-03T03:40:19Z	(#ohihfkq) @<maya https://maya.land/assets/twtxt.txt> you got starlink?
2022-09-03T04:31:15Z	(#miamuaa) @<ocdtrekkie https://twtxt.net/user/ocdtrekkie/twtxt.txt> If your doing something like a chat with people from twtxt and yarn I would join some time if I can whenver you do it next.
2022-09-03T14:44:09Z	(#wtfnszq) @<movq https://www.uninformativ.de/twtxt.txt> Did you time travel into the future to post those? Anyway that is weird and if the system clock is fine then the posts should more be in the future.
2022-09-03T14:50:29Z	(#q7chvzq) @<ocdtrekkie https://twtxt.net/user/ocdtrekkie/twtxt.txt> I could not join because the chat was at 12:30 or 1:00 am for me. Well if I do end up joining sometime I don't use windows and have not used windows in months. Don't have anything to say about windows 11 because I never used it and have not used windows in a few months or more.
2022-09-03T16:52:35Z	(#wtfnszq) @<movq https://www.uninformativ.de/twtxt.txt> Yeah in the US the clocks did not change yet. It switches in Novemeber.
2022-09-03T17:44:31Z	(#wtfnszq) @<movq https://www.uninformativ.de/twtxt.txt> Yeah its a mystery why that would happen
2022-09-03T17:46:31Z	(#q7chvzq) @<ocdtrekkie https://twtxt.net/user/ocdtrekkie/twtxt.txt> That is cool that there is no tpic. Its not like people on these open platforms have to use linux or some free and open os. Anyone can use whatever they want to. Yarn is just a web ui for twtxt and the os does not matter. I can't say on for long time at midnight and I left before you started.
2022-09-03T23:51:42Z	(#wtfnszq) @<mckinley https://twtxt.net/user/mckinley/twtxt.txt> We need to make something like computer time or something like that so people using a computer can say like lets have a chat thursday at 7:00 and no timezone things.
2022-09-04T01:34:03Z	(#wtfnszq) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Though no one ues it though
2022-09-05T04:57:11Z	(#wtfnszq) @<prologic https://twtxt.net/user/prologic/twtxt.txt> That is confusing and no one will understand how to use it. But I did not even know UTC means Coordinated Universal Time. We need to make a revolution to get rid of timezones on the internet and just use UTC
2022-09-06T00:38:38Z	(#wtfnszq) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Yeah its never going to change but its an option to use UTC
2022-09-07T00:47:22Z	(#36mwl6a) @<akoizumi https://social.kyoko-project.wer.ee/user/akoizumi/twtxt.txt> I don't really like the way openbsd does things and use Debian on all my servers. But on the desktop side I like it but don't use it
2022-09-07T20:44:38Z	(#36mwl6a) @<akoizumi https://social.kyoko-project.wer.ee/user/akoizumi/twtxt.txt> What I don't like is that openbsd is secure and then that means some things are different from like debian. Sometimes the security maens some change or whatever has to be done when on debian nothing additional has to be done.
2022-10-28T20:37:01Z	I am back on twtxt. Switched to endevour os from fedora nd setup Jenny again. Now I just have to setup autopublishing and it will be all setup.
2022-10-28T23:46:50Z	(#l33rxrq) @<prologic https://twtxt.net/user/prologic/twtxt.txt>
thanks
2022-10-30T23:18:13Z	(#jhkxlza) @<prologic https://twtxt.net/user/prologic/twtxt.txt>
If there was something that could hide the ip from pings and searches like cloudflare but without cloudlfare that would be great.
2022-10-30T23:28:31Z	(#7wdrrwa) @<movq https://www.uninformativ.de/twtxt.txt>
I have a cheap basic just alarm clock and nothing else. It tells the time and gives an alarm nothing else and it works 100% of the time.
2022-10-30T23:31:20Z	(#asg6sqq) @<prologic https://twtxt.net/user/prologic/twtxt.txt>
Just something to hide the ip when you ping like cloudflare does but not cloudflare. if there even is something like that
2023-12-03T18:56:12-05:00	I am back on twtxt for now. I am using twtwt client. Don't think that it does replies so I should try jenny with mutt again.
2023-12-03T20:26:41-05:00	@<prologic https://twtxt.net/user/prologic/twtxt.txt> @<xuu https://txt.sour.is/user/xuu/twtxt.txt> Don't think I can reply to the thread in twtwt. Right now Jenny is not working for some reason. I wonder if @<movq https://www.uninformativ.de/twtxt.txt> has any ideas. Anyway I am happy to be back and will see if I can get jenny working. Though my following list is gone now. Plus I can't see when someone mentions me if I am not follwing them so I should work on that.
2023-12-03T20:51:03-05:00	(#ghidsjq) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Does putting the hash in my reply work?
2023-12-03T22:03:13-05:00	(#ghidsjq) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Great now I can start using twtxt and reply.
2023-12-04T15:18:10-05:00	(#ghidsjq) @<movq https://www.uninformativ.de/twtxt.txt> Its more than one line I can email it to you if you like
2023-12-04T16:44:39-05:00	(#ghidsjq) @<movq https://www.uninformativ.de/twtxt.txt> Did you get the email I sent? I found two emails and did not know what one to send to.
2023-12-05T20:50:14Z	Testing out Jenny. If this works I will setup Jenny with mutt.
2023-12-06T20:40:43Z	(#mniqw3q) @<prologic https://twtxt.net/user/prologic/twtxt.txt> That is good even though I do see mutt getting confusing with all the replies and threads.
2023-12-07T22:08:23Z	(#np2awqq) @<prologic https://twtxt.net/user/prologic/twtxt.txt>
I saw that but then I refreshed the ublock lists and I could watch.
2023-12-08T00:14:15Z	(#mniqw3q) @<movq https://www.uninformativ.de/twtxt.txt>
Yes I have threading on. I wanted to put new posts at the top with set sort_aux = reverse-last-date-sent but that that makes the threads do the newest first not bellow the reply. So all replies are in a top newest order. But I can just use sort date_sent and then go to the end to go to the newest post.
2023-12-09T21:53:56Z	(#yv7ccjq) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Thanks I am glad to be here. Was here before but now I am back.
2023-12-23T22:48:14Z	Don't install Trinity desktop on Slackware. Kde wayland does not work anymore and regular kde x11 is mixed with parts from Trinity. Going to need to do a reinstall. Plus Trinity is half working anyway. Best to stick with Kde and xfce.
2023-12-24T20:16:42Z	(#u7tbolq) @<lyse https://lyse.isobeef.org/twtxt.txt> I use Jenny and I thought the multi line did work. Will keep that in mind when I am writing out posts. Also do you know how I can see if someone mentions me that I am not follwing? I know the user agents exist but I can't view the server lgos since I host this on codeberg but if I do switch to my own server it would work.
2023-12-25T13:56:52Z	(#u7tbolq) @<lyse https://lyse.isobeef.org/twtxt.txt> Yeah I think jenny should work fine. Might have got messed up. Don't know what messed up my file I don't recall anything that would mess up my file.  Yep the search engine is an option but if I switch to my own server or get a rented server then I will be able to view the logs.
2023-12-25T13:58:20Z	(#u7tbolq) @<prologic https://twtxt.net/user/prologic/twtxt.txt> Yarnd exposes it for the users to view logs for there own feed?
2023-12-26T03:35:58Z	(#u7tbolq) @<lyse https://lyse.isobeef.org/twtxt.txt> That is good so that the user can see if someone replies or mentions him. I might have to switch my website to a server I can view logs.
2023-12-27T05:19:33Z	(#u7tbolq) @<prologic https://twtxt.net/user/prologic/twtxt.txt> It would and I should switch over and then I can view the logs.
2023-12-28T06:03:38Z	(#pq4irfa) @<lyse https://lyse.isobeef.org/twtxt.txt> That sunset looks really nice.
